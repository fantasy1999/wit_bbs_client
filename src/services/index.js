import axios from 'axios'

export default () => {
  return axios.create({
    baseURL: 'https://wit-bbs-api-staging.herokuapp.com/'
  })
}
