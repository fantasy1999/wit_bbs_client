import Vue from 'vue'
import VueRouter from 'vue-router'
import Global from '../views/layout/global'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: Global,
    children: [
      { path: '', name: 'Default', component: () => import('../views/post/list') }
    ]
  },
  {
    path: '/post',
    component: Global,
    children: [
      { path: 'create', name: 'post-create', component: () => import('../views/post/create') },
      { path: 'list', name: 'post-list', component: () => import('../views/post/list') },
      { path: 'detail/:id', name: 'post-detail', component: () => import('../views/post/detail') },
      { path: 'search/:keywords', name: 'post-search', component: () => import('../views/post/search') }
    ]
  },
  {
    path: '/user',
    component: Global,
    children: [
      { path: 'update', name: 'user-update', component: () => import('../views/user/update') }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
