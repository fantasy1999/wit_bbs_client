describe('Homepage test', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080/', { timeout: 30000 })
  })
  it('Visits the app root url', () => {
    cy.contains('h1', 'Discover')
    cy.contains('span', 'Log in')
    cy.contains('span', 'Sign up')
  })
  it('should search related post', function () {
    cy.get('#search')
      .type('title{enter}', { force: true })
    cy.url().should('include', 'search')
    cy.url().should('include', 'title')
    cy.contains('h1', 'Search Result')
  })
  it('should browse the post detail', function () {
    cy.get('.title').eq(0).click()
    cy.url().should('include', 'detail')
    cy.contains('Author')
    cy.get('#btn-comment').contains('comment')
  })
  it('should alert login dialog', function () {
    cy.contains('Log in').click()
    cy.contains('span', 'Log in')
    cy.contains('span', 'New to Community?')
    cy.contains('a', 'SIGN UP')
  })
  it('should alert sign up dialog', function () {
    cy.contains('Sign up').click()
    cy.contains('span', 'Sign up')
    cy.contains('span', 'Already a user?')
    cy.contains('a', 'LOG IN')
    cy.get('body > div.el-dialog__wrapper > div > div.el-dialog__body > section:nth-child(2) > section > form > div').should('have.length', 4)
  })
  it('should login successfully', function () {
    cy.contains('Log in').click()
    cy.get('input[name="username"]').eq(0).type('test')
    cy.get('input[name="password"]').eq(0).type('12345678')
    cy.contains('LOG IN').click()
    cy.contains('Create')
  })
  it('should failed to register', function () {
    cy.contains('Sign up').click()
    cy.get('input[name="username"]').eq(1).type('test')
    cy.get('input[name="email"]').eq(0).type('test@test.com')
    cy.get('input[name="password"]').eq(1).type('12345678')
    cy.get('input[name="comparePassword"]').eq(0).type('12345678')
    // cy.contains('SIGN UP').click()
    cy.get('[style="text-align: center;"] > .dialog-footer > .el-button').click()
    cy.get('.el-message__content').contains('Failed to register: username has been used!')
  })
})

describe('Post, comment, update info and logout Test after login', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080/', { timeout: 30000 })
    cy.contains('Log in').click()
    cy.get('input[name="username"]').eq(0).type('test')
    cy.get('input[name="password"]').eq(0).type('12345678')
    cy.contains('LOG IN').click()
  })
  it('should create a post successfully', function () {
    cy.contains('Create').click({ force: true })
    cy.get('input[name="title"]').eq(0).type('test post')
    cy.get('.el-textarea__inner').eq(0).type('This is a test post')
    cy.contains('SUBMIT').click()
    cy.url().should('include', '/post/list')
    cy.contains('test post')
  })
  it('should reset the create form', function () {
    cy.contains('Create').click({ force: true })
    cy.get('input[name="title"]').eq(0).type('test post')
    cy.get('.el-textarea__inner').eq(0).type('This is a test post')
    cy.contains('RESET').click()
    cy.get('input[name="title"]').eq(0).should('have.value', '')
    cy.get('.el-textarea__inner').eq(0).should('have.value', '')
  })
  // it('should comment successfully', function () {
  //   cy.get('.title').eq(0).click()
  //   cy.get('#container > div.main-container.el-row > div.el-col.el-col-24.el-col-xs-24.el-col-sm-16.el-col-md-16.el-col-lg-16.el-col-xl-16 > div > div.comment-container > div.comment-input > div > textarea').eq(0).type('This is a comment')
  //   cy.get('#btn-comment').click()
  //   cy.get('.el-message__content').contains('Comment successfully')
  //   cy.contains('This is a comment')
  // })
  it('should logout successfully', function () {
    cy.get('.el-dropdown').click({ force: true })
    cy.contains('Logout').click({ force: true })
    cy.get('.el-message__content').contains('Logout successfully')
    cy.contains('Log in')
    cy.contains('Sign up')
  })
  it('should update successfully', function () {
    cy.get('.el-dropdown').click({ force: true })
    cy.contains('Update Info').click({ force: true })
    cy.get('input[name="password"]').eq(0).type('12345678')
    cy.get('input[name="comparePassword"]').eq(0).type('12345678')
    cy.get('.el-button').contains('UPDATE').click()
    cy.get('.el-message__content').contains('Update successfully')
  })
})
