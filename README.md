# Assignment 2 - Agile Software Practice.

Name: Chengming Fan  
ID: 20086437

## Client UI.

![][homepage]

>>Show the lated posts

![][register]

>>register here

![][login]

>>login here

![][create]

>>create a new post


![][detail]

>>Show the post detail

![][comment]

>>comment successfully 

![][search]

>>Show search result

![][update]

>>user can update their info here


![][logout]

>>logout and show the message

## E2E/Cypress testing.



## Web API CI.

[coverage-report](https://fantasy1999.gitlab.io/wit_bbs_api/coverage/lcov-report)
## GitLab CI.




[homepage]: ./img/homepage.png
[login]: ./img/login.png
[register]: ./img/register.png
[create]: ./img/create.png
[detail]: ./img/detail.png
[comment]: ./img/comment.png
[search]: ./img/search.png
[update]: ./img/update_info.png
[logout]: ./img/logout.png
